package com.example.hw6.ui.users

import androidx.lifecycle.ViewModel
import com.example.hw6.data.domain.User
import com.example.hw6.data.repository.UserRepository

class UsersViewModel(private val userRepository: UserRepository) : ViewModel() {


    fun getUsers() = userRepository.getUsers()

    fun addUser(user: User) = userRepository.addUser(user)
}