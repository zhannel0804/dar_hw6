package com.example.hw6.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.hw6.R
import com.example.hw6.data.domain.User
import com.example.hw6.ui.users.UsersViewModel
import com.example.hw6.ui.users.UsersViewModelFactory
import com.example.hw6.utilities.InjectorUtil
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.StringBuilder

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initializeUi()
    }

    private fun initializeUi() {

        val factory = InjectorUtil.provideUserViewModelFactory()

        val viewModel = ViewModelProviders.of(this, factory)
            .get(UsersViewModel::class.java)

        viewModel.getUsers().observe(this, Observer { users ->
            val stringBuilder = StringBuilder()

            users.forEach {user ->
                stringBuilder.append("$user\n\n")
            }
            textView_users.text = stringBuilder.toString()
        })

        button_add_firstname.setOnClickListener {
            val user = User(editText_firstname.text.toString(), editText_lastname.text.toString())

            viewModel.addUser(user)

            editText_firstname.setText("")
            editText_lastname.setText("")
        }
    }
}
