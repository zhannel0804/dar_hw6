package com.example.hw6.utilities

import com.example.hw6.data.db.UserDatabase
import com.example.hw6.data.repository.UserRepository
import com.example.hw6.ui.users.UsersViewModelFactory

object InjectorUtil {

    fun provideUserViewModelFactory(): UsersViewModelFactory {
        val userRepository = UserRepository.getInstance(UserDatabase.getInstance().userDao)
        return UsersViewModelFactory(userRepository)
    }
}