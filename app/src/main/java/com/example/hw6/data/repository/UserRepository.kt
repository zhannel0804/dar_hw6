package com.example.hw6.data.repository

import com.example.hw6.data.domain.User
import com.example.hw6.data.domain.UserDao

class UserRepository private constructor(private val userDao: UserDao){

    fun addUser(user: User) {
        userDao.addUser(user)
    }

    fun getUsers() = userDao.getUsers()

    companion object {
        @Volatile private var instance: UserRepository? = null

        fun getInstance(userDao: UserDao) =
            instance ?: synchronized(this) {
                instance ?: UserRepository(userDao).also { instance = it }
            }
    }
}