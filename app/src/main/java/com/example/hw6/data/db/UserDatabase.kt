package com.example.hw6.data.db

import com.example.hw6.data.domain.UserDao

class UserDatabase private constructor(){
    var userDao = UserDao()
        private set

    companion object {
        @Volatile private var instance: UserDatabase? = null

        fun getInstance() =
            instance ?: synchronized(this) {
                instance ?: UserDatabase().also { instance = it }
            }
    }
}