package com.example.hw6.data.domain

data class User (
    val firstName: String,
    val lastName: String
)